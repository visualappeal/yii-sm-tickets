<?php

/**
 * Controller for tickets.
 *
 * @package Ticket
 */
class TicketController extends Controller
{
	/**
	 * Default layout.
	 *
	 * @var string
	 */
	public $layout = '//layouts/admin';

	/**
	 * Default action for the controller.
	 *
	 * @var string
	 */
	public $defaultAction = 'admin';

	/**
	 * Returns the action filters.
	 *
	 * @access public
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl',
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 *
	 * @access public
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',
				'actions' => array('create', 'update', 'view'),
				'users' => array('@'),
			),
			array('allow',
				'actions' => array('admin', 'delete'),
				'roles' => array(Yii::app()->user->checkAccess(User::LEVEL_BOARD)),
			),
			array('deny',
				'users' => array('*'),
			),
		);
	}

	/**
	 * Displays a particular ticket.
	 *
	 * @param integer $id the ID of the ticket to be displayed
	 *
	 * @access public
	 * @return void
	 */
	public function actionView($id)
	{
		$model = $this->_loadModel($id, array(), array('root'));
		if ($this->module->canViewTicket($model) === false)
			throw new CHttpException(403, Yii::t('Ticket', 'Sie sind nicht berechtigt das Ticket zu sehen!'));

		$ticket = new Ticket('response');

		if (isset($_POST['Ticket'])) {
			$ticket->attributes = $_POST['Ticket'];
			$ticket->parent = $model;
			$ticket->user = Yii::app()->user->id;

			if ($ticket->save()) {
				$this->redirect(array('view', 'id' => $id, '#' => $ticket->id));
			}
		}

		$this->render(
			'view',
			array(
				'ticket' => $ticket,
				'model' => $model,
			)
		);
	}

	/**
	 * Creates a new ticket.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 *
	 * @access public
	 * @return void
	 */
	public function actionCreate()
	{
		$model = new Ticket;

		if (isset($_POST['Ticket'])) {
			$model->attributes = $_POST['Ticket'];
			$model->user = Yii::app()->user->id;

			if($model->save())
				$this->redirect(array('view', 'id' => $model->id));
		}

		$this->render(
			'create',
			array(
				'model'=>$model,
			)
		);
	}

	/**
	 * Updates a particular ticket.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param integer $id the ID of the ticket to be updated
	 *
	 * 
	 */
	public function actionUpdate($id)
	{
		$model = $this->_loadModel($id);
		if ($this->module->canUpdateTicket($model) === false)
			throw new CHttpException(403, Yii::t('Ticket', 'Sie sind nicht berechtigt das Ticket zu sehen!'));

		if (isset($_POST['Ticket'])) {
			$model->attributes = $_POST['Ticket'];

			if ($model->save())
				$this->redirect(array('view', 'id' => $model->id));
		}

		$this->render(
			'update',
			array(
				'model' => $model,
			)
		);
	}

	/**
	 * Deletes a particular ticket.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 *
	 * @param integer $id the ID of the ticket to be deleted
	 *
	 * @access public
	 * @return void
	 */
	public function actionDelete($id)
	{
		$this->_loadModel($id)->delete();

		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Manages all tickets.
	 *
	 * @access public
	 * @return void
	 */
	public function actionAdmin()
	{
		$model = new Ticket('search');
		$model->unsetAttributes();

		if (isset($_GET['Ticket']))
			$model->attributes = $_GET['Ticket'];

		$this->render(
			'admin',
			array(
				'model' => $model,
			)
		);
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 *
	 * @param integer the ID of the model to be loaded
	 *
	 * @access private
	 * @return Ticket
	 */
	private function _loadModel($id, $with = array(), $scopes = array())
	{
		$model = Ticket::model()->with($with)->findByPk($id, array('scopes' => $scopes));

		if(is_null($model))
			throw new CHttpException(404, Yii::t('Ticket', 'Das Ticket #{id} existiert nicht!', array('{id}' => $id)));

		return $model;
	}
}
