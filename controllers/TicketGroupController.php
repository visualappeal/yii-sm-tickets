<?php

class TicketGroupController extends Controller
{
	/**
	 * Default controller layout.
	 *
	 * @access public
	 * @var string
	 */
	public $layout = '//layouts/admin';

	/**
	 * Get the action filters.
	 *
	 * @access public
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 *
	 * @access public
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',
				'actions' => array('create', 'update', 'admin', 'delete', 'view'),
				'roles' => array(User::LEVEL_BOARD),
			),
			array('deny',
				'users' => array('*'),
			),
		);
	}

	/**
	 * Displays all tickets in a group.
	 *
	 * @param integer $id the ID of the model to be displayed
	 *
	 * @access public
	 * @return void
	 */
	public function actionView($id)
	{
		$model = $this->_loadModel($id);

		$ticketProvider = new CActiveDataProvider(
			'Ticket', 
			array(
				'criteria' => array(
					'condition' => 'ticket_group_id = :group_id',
					'params' => array(
						':group_id' => $model->id,
					)
				)
			)
		);

		$ticket = new Ticket('search');
		$ticket->unsetAttributes();

		if (isset($_GET['Ticket']))
			$ticket->attributes = $_GET['Ticket'];

		$this->render(
			'view',
			array(
				'model' => $model,
				'ticket' => $ticket,
				'ticketProvider' => $ticketProvider
			)
		);
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model = new TicketGroup;

		if (isset($_POST['TicketGroup'])) {
			$model->attributes = $_POST['TicketGroup'];

			if ($model->save()) {
				Yii::app()->user->setFlash(
					'success',
					Yii::t(
						'Ticket.Category',
						'Die Ticket-Kategorie {title} wurde erstellt.',
						array(
							'{title}' => $model->title,
						)
					)
				);

				$this->redirect(array('admin'));
			}
		}

		$this->render(
			'create',
			array(
				'model' => $model,
			)
		);
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param integer $id the ID of the model to be updated
	 *
	 * @access public
	 * @return void
	 */
	public function actionUpdate($id)
	{
		$model=$this->_loadModel($id);

		if (isset($_POST['TicketGroup'])) {
			$model->attributes = $_POST['TicketGroup'];

			if($model->save())
				$this->redirect(array('view','id' => $model->id));
		}

		$this->render(
			'update',
			array(
				'model' => $model,
			)
		);
	}

	/**
	 * Deletes a particular group.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 *
	 * @param integer $id the ID of the group to be deleted
	 *
	 * @access public
	 * @return void
	 */
	public function actionDelete($id)
	{
		$this->_loadModel($id)->delete();

		if (!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Manages all groups.
	 *
	 * @access public
	 * @return void
	 */
	public function actionAdmin()
	{
		$model = new TicketGroup('search');
		$model->unsetAttributes();

		if (isset($_GET['TicketGroup']))
			$model->attributes = $_GET['TicketGroup'];

		$this->render(
			'admin',
			array(
				'model' => $model,
			)
		);
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 *
	 * @param integer the ID of the model to be loaded
	 *
	 * @access public
	 * @return TicketGroup
	 */
	private function _loadModel($id, $with = array())
	{
		$model = TicketGroup::model()->with($with)->findByPk($id);

		if (is_null($model))
			throw new CHttpException(404, Yii::t('Ticket.Group', 'Die Kategorie {id} konnte nicht gefunden werden', array('{id}' => $id)));

		return $model;
	}
}
