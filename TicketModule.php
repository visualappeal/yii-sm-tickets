<?php

/**
 * Ticket Module.
 *
 * @package Ticket
 */
class TicketModule extends CWebModule {
	const API_DEFAULT = 0;
	const API_CONTACT = 1;
	const API_WIDGET = 2;

	/**
	 * Init application.
	 *
	 * @access protected
	 * @return void
	 */
	protected function init()
	{
		Yii::import('application.modules.ticket.models.*');

		if (isset(Yii::app()->feedback)) {
			Yii::app()->feedback->onFeedback = array($this, 'addFeedback');
		}

		return parent::init();
	}

	/**
	 * Register WYSIWYG script and layout files.
	 *
	 * @access public
	 * @return void
	 */
	public function registerTextEditor()
	{
		$cs = Yii::app()->clientScript;

		$cs->registerScriptFile(Yii::app()->request->baseUrl . '/js/redactor/redactor.min.js');
		$cs->registerScriptFile(Yii::app()->request->baseUrl . '/js/redactor/de.js');
		$cs->registerScriptFile(Yii::app()->request->baseUrl . '/js/redactor/fullscreen.js');
		$cs->registerCssFile(Yii::app()->request->baseUrl . '/js/redactor/redactor.css');
	}

	/**
	 * Register datetime picker.
	 *
	 * @access public
	 * @return void
	 */
	public function registerDateTime()
	{

	}

	/**
	 * Add a ticket.
	 *
	 * @param array $attributes
	 *
	 * @access public
	 * @return boolean Whether the ticket was saved
	 */
	public function addTicket($attributes)
	{
		$ticket = new Ticket('api');
		$ticket->attributes = $attributes;

		return $ticket->save();
	}

	/**
	 * Event handler for feedback creation.
	 *
	 * @param CEvent $event
	 *
	 * @access public
	 * @return void
	 */
	public function addFeedback($event)
	{
		$ticket = new Ticket('api');
		$ticket->title = 'Feedback';
		if (!$ticket->save()) {
			Yii::log('Feedback could not be saved: ' . serialize($ticket->errors), 'error', 'application.modules.ticket.TicketModule');
		} else {
			Yii::trace('Feedback saved!', 'application.modules.ticket.TicketModule');
		}
	}

	/**
	 * Get API ID for API string.
	 *
	 * @param string $api
	 *
	 * @access public
	 * @return integer
	 */
	public function getApiId($api)
	{
		switch ($api) {
			case 'Contact Form':
				return self::API_CONTACT;
			case 'Widget':
				return self::API_WIDGET;
			default:
				return self::API_DEFAULT;
		}
	}

	/**
	 * Check whether the user has the rights to view the ticket.
	 *
	 * @param Ticket $model
	 *
	 * @access public
	 * @return boolean
	 */
	public function canViewTicket(Ticket $model)
	{
		if (Yii::app()->user->checkAccess(User::LEVEL_BOARD))
			return true;

		if (Yii::app()->user->id === $model->user_id)
			return true;

		if (Yii::app()->user->id === $model->user_assigned)
			return true;

		return false;
	}

	/**
	 * Check whether the user has the rights to update the ticket.
	 *
	 * @param Ticket $model
	 *
	 * @access public
	 * @return boolean
	 */
	public function canUpdateTicket(Ticket $model)
	{
		if (Yii::app()->user->checkAccess(User::LEVEL_BOARD))
			return true;

		if (Yii::app()->user->id === $model->user_id)
			return true;

		if (Yii::app()->user->id === $model->user_assigned)
			return true;

		return false;
	}

	/**
	 * Get dropdown values for the user assignment dropdown.
	 *
	 * @access public
	 * @return array
	 */
	public function getUserDropdown()
	{
		return CHtml::listData(User::model()->board()->findAll(), 'id', 'name');
	}
}

?>