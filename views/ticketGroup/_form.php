<?php
/* @var $this TicketGroupController */
/* @var $model TicketGroup */
/* @var $form CActiveForm */

$this->module->registerTextEditor();

$form = $this->beginWidget(
	'EBootstrapActiveForm', 
	array(
		'id' => 'ticket-group-form',
		'horizontal' => true,
	)
); 

?>

	<?php echo $form->beginControlGroup($model, 'title'); ?>
		<?php echo $form->labelEx($model, 'title'); ?>
		<?php echo $form->beginControls($model, 'title'); ?>
			<?php echo $form->textField($model, 'title'); ?>
			<?php echo $form->error($model, 'title'); ?>
		<?php echo $form->endControls($model, 'title'); ?>
	<?php echo $form->endControlGroup($model, 'title'); ?>

	<?php echo $form->beginControlGroup($model, 'description'); ?>
		<?php echo $form->labelEx($model, 'description'); ?>
		<?php echo $form->beginControls($model, 'description'); ?>
			<?php echo $form->textArea($model, 'description', array('class' => 'texteditor')); ?>
			<?php echo $form->error($model, 'description'); ?>
		<?php echo $form->endControls($model, 'description'); ?>
	<?php echo $form->endControlGroup($model, 'description'); ?>

	<?php echo $form->beginActions(); ?>
		<?php echo Ebootstrap::submitButton($model->isNewRecord ? Yii::t('Ticket.Group', 'Erstellen') : Yii::t('Ticket.Group', 'Speichern'), 'primary', '', false, $model->isNewRecord ? 'plus' : 'ok', true); ?>
	<?php echo $form->endActions() ?>

<?php $this->endWidget();