<?php
/* @var $this TicketGroupController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Ticket Groups',
);

$this->menu=array(
	array('label'=>'Create TicketGroup', 'url'=>array('create')),
	array('label'=>'Manage TicketGroup', 'url'=>array('admin')),
);
?>

<h1>Ticket Groups</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
