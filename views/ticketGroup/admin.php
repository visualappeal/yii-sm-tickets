<?php
/* @var $this TicketGroupController */
/* @var $model TicketGroup */

$this->breadcrumbs = array(
	Yii::t('Ticket', 'Tickets') => array('/ticket/ticket/admin'),
	Yii::t('Ticket.Group', 'Kategorien'),
);

$this->beginWidget(
	'AdminWidget', 
	array(
		'title' => Yii::t('Ticket.Group', 'Kategorien verwalten'),
		'buttons' => array(
			EBootstrap::ibutton(Yii::t('Ticket.Group', 'erstellen'), array('/ticket/ticketGroup/create'), '', 'mini', '', 'plus', false, array('title' => Yii::t('Ticket.Group', 'Kategorie erstellen')))
		),
		'icon' => 'th-large',
		'table' => true,
	)
);

$this->widget(
	'EBootstrapGridView', 
	array(
		'id' => 'ticket-group-grid',
		'dataProvider' => $model->search(),
		'filter' => $model,
		'columns' => array(
			'id',
			'title',
			array(
				'class' => 'EBootstrapButtonColumn',
			),
		),
	)
);

$this->endWidget();