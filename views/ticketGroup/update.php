<?php
/* @var $this TicketGroupController */
/* @var $model TicketGroup */

$this->breadcrumbs = array(
	Yii::t('Ticket', 'Tickets') => array('/ticket/ticket/admin'),
	Yii::t('Ticket.Group', 'Kategorien') => array('admin'),
	$model->title,
	Yii::t('Ticket.Group', 'Bearbeiten')
);

$this->beginWidget(
	'AdminWidget', 
	array(
		'title' => Yii::t('Ticket.Group', 'Kategorie: {title}', array('{title}' => $model->title)),
		'buttons' => array(
			EBootstrap::ibutton(Yii::t('Ticket.Group', 'anzeigen'), array('/ticket/ticketGroup/view', 'id' => $model->id), '', 'mini', '', 'search', false, array('title' => Yii::t('Ticket.Group', 'Kategorie anzeigen')))
		),
		'icon' => 'search',
	)
);

$this->renderPartial(
	'_form', 
	array(
		'model' => $model
	)
);

$this->endWidget();