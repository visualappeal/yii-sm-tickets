<?php
/* @var $this TicketGroupController */
/* @var $model TicketGroup */

$this->breadcrumbs = array(
	Yii::t('Ticket', 'Tickets') => array('/ticket/ticket/admin'),
	Yii::t('Ticket.Group', 'Kategorien') => array('admin'),
	$model->title,
);

$this->beginWidget(
	'AdminWidget', 
	array(
		'title' => Yii::t('Ticket.Group', 'Kategorie: {title}', array('{title}' => $model->title)),
		'buttons' => array(
			EBootstrap::ibutton(Yii::t('Ticket.Group', 'bearbeiten'), array('/ticket/ticketGroup/update', 'id' => $model->id), '', 'mini', '', 'pencil', false, array('title' => Yii::t('Ticket.Group', 'Kategorie bearbeiten')))
		),
		'icon' => 'search',
	)
);

$this->widget(
	'EBootstrapGridView', 
	array(
		'id' => 'ticket-grid',
		'dataProvider' => $ticketProvider,
		'filter' => $ticket,
		'columns' => array(
			'id',
			'userDisplay',
			'title',
			'created',
			'updated',
			'status',
			array(
				'class' => 'EBootstrapButtonColumn',
			),
		),
	)
);

$this->endWidget();