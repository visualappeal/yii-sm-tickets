<?php
/* @var $this TicketGroupController */
/* @var $model TicketGroup */

$this->breadcrumbs=array(
	Yii::t('Ticket', 'Tickets') => array('/ticket/ticket/admin'),
	Yii::t('Ticket.Group', 'Kategorien') => array('admin'),
	Yii::t('Ticket.Group', 'Erstellen'),
);

$this->beginWidget(
	'AdminWidget', 
	array(
		'title' => Yii::t('Ticket.Group', 'Kategorie erstellen'),
		'icon' => 'plus',
	)
);

$this->renderPartial(
	'_form', 
	array(
		'model' => $model
	)
);

$this->endWidget();