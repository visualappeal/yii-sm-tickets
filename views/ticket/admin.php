<?php
/* @var $this TicketController */
/* @var $model Ticket */

$this->breadcrumbs = array(
	Yii::t('Ticket', 'Tickets'),
);

$this->beginWidget(
	'AdminWidget', 
	array(
		'title' => Yii::t('Ticket', 'Tickets verwalten'),
		'buttons' => array(
			EBootstrap::ibutton(Yii::t('Ticket', 'erstellen'), array('/ticket/ticket/create'), '', 'mini', '', 'plus', false, array('title' => Yii::t('Ticket', 'Ticket erstellen')))
		),
		'icon' => 'th-list',
		'table' => true,
	)
);

function getTicketRowCssClass($this, $row, $data) {
	if (!empty($data->deadline)) {
		$deadline = strtotime($data->deadline);

		if ($deadline < time())
			return 'error';

		if (sqrt(pow($deadline-time(), 2)) < 3600*24)
			return 'warning';
	}

	switch ($data->status) {
		case Ticket::STATUS_NEW:
			return 'info';
		case Ticket::STATUS_CLOSED:
			return 'success';
		default:
			return '';
	}
}

$this->widget(
	'EBootstrapGridView', 
	array(
		'id' => 'ticket-grid',
		'dataProvider' => $model->search(),
		'filter' => $model,
		'columns' => array(
			'id',
			'title',
			array(
				'name' => 'ticket_group_id',
				'filter' => $model->groups,
				'value' => 'isset($data->theGroup->title) ? $data->theGroup->title : ""'
			),
			'userDisplay',
			array(
				'name' => 'created',
				'filter' => false,
			),
			array(
				'name' => 'deadline',
				'filter' => false,
			),
			array(
				'name' => 'status',
				'filter' => $model->statusDropdown,
				'value' => '$data->statusMessage',
			),
			array(
				'class' => 'EBootstrapButtonColumn',
			),
		),
		'rowCssClassExpression' => 'getTicketRowCssClass($this, $row, $data)'
	)
);

$this->endWidget();