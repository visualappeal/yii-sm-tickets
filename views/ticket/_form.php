<?php
/* @var $this TicketController */
/* @var $model Ticket */
/* @var $form CActiveForm */

$cs = Yii::app()->clientScript;

//Datetime-Picker
$jsFile = Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('application.modules.ticket.js').'/timepicker.js');
$cs->registerScriptFile($jsFile, CClientScript::POS_END);

$cs->registerScript('ticket-datepicker', "
$.timepicker.regional['de'] = {
	timeOnlyTitle: 'Uhrzeit auswählen',
	timeText: 'Zeit',
	hourText: 'Stunde',
	minuteText: 'Minute',
	currentText: 'Jetzt',
	closeText: 'Auswählen',
	ampm: false,
	dateFormat: 'dd.mm.yy',
	timeFormat: 'hh:mm'
};

$.timepicker.setDefaults($.timepicker.regional['de']);

$('.datetimepicker').datetimepicker();
");

$form = $this->beginWidget(
	'EBootstrapActiveForm', 
	array(
		'id' => 'ticket-form',
		'horizontal' => true,
	)
);

$this->module->registerTextEditor();
$this->module->registerDatetime();

?>
	<?php if ($model->scenario != 'response'): ?>
		<?php echo $form->beginControlGroup($model, 'title'); ?>
			<?php echo $form->labelEx($model, 'title'); ?>
			<?php echo $form->beginControls($model, 'title'); ?>
				<?php echo $form->textField($model, 'title'); ?>
				<?php echo $form->error($model, 'title'); ?>
			<?php echo $form->endControls($model, 'title'); ?>
		<?php echo $form->endControlGroup($model, 'title'); ?>

		<?php echo $form->beginControlGroup($model, 'ticket_group_id'); ?>
			<?php echo $form->labelEx($model, 'ticket_group_id'); ?>
			<?php echo $form->beginControls($model, 'ticket_group_id'); ?>
				<?php echo $form->dropDownList($model, 'ticket_group_id', $model->groups, array('empty' => Yii::t('Ticket', 'Keine Kategorie'))); ?>
				<?php echo $form->error($model, 'ticket_group_id'); ?>
			<?php echo $form->endControls($model, 'ticket_group_id'); ?>
		<?php echo $form->endControlGroup($model, 'ticket_group_id'); ?>

		<?php echo $form->beginControlGroup($model, 'status'); ?>
			<?php echo $form->labelEx($model, 'status'); ?>
			<?php echo $form->beginControls($model, 'status'); ?>
				<?php echo $form->dropDownList($model, 'status', $model->statusDropdown); ?>
				<?php echo $form->error($model, 'status'); ?>
			<?php echo $form->endControls($model, 'status'); ?>
		<?php echo $form->endControlGroup($model, 'status'); ?>

		<?php echo $form->beginControlGroup($model, 'user_assigned'); ?>
			<?php echo $form->labelEx($model, 'user_assigned'); ?>
			<?php echo $form->beginControls($model, 'user_assigned'); ?>
				<?php echo $form->dropDownList($model, 'user_assigned', $this->module->userDropdown, array('empty' => Yii::t('Ticket', 'Niemand'))); ?>
				<?php echo $form->error($model, 'user_assigned'); ?>
			<?php echo $form->endControls($model, 'user_assigned'); ?>
		<?php echo $form->endControlGroup($model, 'user_assigned'); ?>

		<?php echo $form->beginControlGroup($model, 'deadline'); ?>
			<?php echo $form->labelEx($model, 'deadline'); ?>
			<?php echo $form->beginControls($model, 'deadline'); ?>
				<?php echo $form->textField($model, 'deadline', array('class' => 'datetimepicker')); ?>
				<?php echo $form->error($model, 'deadline'); ?>
			<?php echo $form->endControls($model, 'deadline'); ?>
		<?php echo $form->endControlGroup($model, 'deadline'); ?>
	<?php endif; ?>

	<?php echo $form->beginControlGroup($model, 'description'); ?>
		<?php echo $form->labelEx($model, 'description'); ?>
		<?php echo $form->beginControls($model, 'description'); ?>
			<?php echo $form->textArea($model, 'description', array('class' => 'texteditor')); ?>
			<?php echo $form->error($model, 'description'); ?>
		<?php echo $form->endControls($model, 'description'); ?>
	<?php echo $form->endControlGroup($model, 'description'); ?>

	<?php echo $form->beginActions(); ?>
		<?php echo EBootstrap::submitButton($model->isNewRecord ? Yii::t('Ticket', 'Erstellen') : Yii::t('Ticket', 'Speichern'), 'primary', '', false, $model->isNewRecord ? 'plus' : 'ok', true); ?>
	<?php echo $form->endActions(); ?>

<?php $this->endWidget(); ?>