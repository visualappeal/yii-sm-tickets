<?php
/* @var $data Ticket */

$buttons = array();

if (Yii::app()->user->checkAccess(User::LEVEL_BOARD)) {
	$buttons[] = EBootstrap::ibutton(Yii::t('Ticket', 'bearbeiten'), array('/ticket/ticket/update', 'id' => $data->id), '', 'mini', '', 'pencil', false, array('title' => Yii::t('Ticket', 'Ticket bearbeiten')));
}
?>

<?php
$this->beginWidget(
	'AdminWidget', 
	array(
		'title' => Yii::t('Ticket', 'Kommentar #{id}', array('{id}' => $data->id)) . '<a name="' . $data->id . '"></a>',
		'buttons' => $buttons,
		'icon' => 'comment',
	)
);
?>

<p class="muted"><?php echo Yii::t('Ticket', '{user} schrieb am {date}', array('{user}' => $data->userDisplay, '{date}' => $data->created)); ?></p>

<div class="well">
	<?php echo $data->description; ?>
</div>

<?php
$this->endWidget();