<?php
/* @var $this TicketController */
/* @var $model Ticket */

$this->breadcrumbs = array(
	Yii::t('Ticket', 'Tickets') => array('admin'),
	Yii::t('Ticket', 'Erstellen'),
);

$this->beginWidget(
	'AdminWidget', 
	array(
		'title' => Yii::t('Ticket', 'Ticket erstellen'),
		'icon' => 'plus',
	)
);

$this->renderPartial(
	'_form', 
	array(
		'model' => $model
	)
);

$this->endWidget();