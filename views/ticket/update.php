<?php
/* @var $this TicketController */
/* @var $model Ticket */

$this->breadcrumbs=array(
	Yii::t('Ticket', 'Tickets') => array('admin'),
	'#' . $model->id => array('view', 'id' => $model->id),
	Yii::t('Ticket', 'Bearbeiten'),
);

$this->beginWidget(
	'AdminWidget', 
	array(
		'title' => Yii::t('Ticket', 'Bearbeite Ticket #{id}: {title}', array('{id}' => $model->id, '{title}' => $model->title)),
		'buttons' => array(
			EBootstrap::ibutton(Yii::t('Ticket', 'anzeigen'), array('/ticket/ticket/view', 'id' => $model->id), '', 'mini', '', 'search', false, array('title' => Yii::t('Ticket', 'Ticket anzeigen'))),
		),
		'icon' => 'th-list',
	)
);

$this->renderPartial(
	'_form', 
	array(
		'model' => $model
	)
);

$this->endWidget();