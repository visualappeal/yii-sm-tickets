<?php
/* @var $this TicketController */
/* @var $model Ticket */

$this->breadcrumbs=array(
	Yii::t('Ticket', 'Tickets') => array('admin'),
	'#' . $model->id . ': ' . $model->title,
);

$this->beginWidget(
	'AdminWidget', 
	array(
		'title' => Yii::t('Ticket', 'Ticket #{id}: {title}', array('{id}' => $model->id, '{title}' => $model->title)),
		'buttons' => array(
			EBootstrap::ibutton(Yii::t('Ticket', 'erstellen'), array('/ticket/ticket/create'), '', 'mini', '', 'plus', false, array('title' => Yii::t('Ticket', 'Ticket erstellen'))),
			EBootstrap::ibutton(Yii::t('Ticket', 'bearbeiten'), array('/ticket/ticket/update', 'id' => $model->id), '', 'mini', '', 'pencil', false, array('title' => Yii::t('Ticket', 'Ticket bearbeiten')))
		),
		'icon' => 'search',
		
	)
);
?>

<div class="row">
	<div class="col-sm-6">
		<table class="table">
			<tbody>
				<tr>
					<td class="bold">
						<?php echo $model->getAttributeLabel('statusMessage'); ?>
					</td>
					<td>
						<?php echo $model->statusMessage; ?>
					</td>
				</tr>

				<tr>
					<td class="bold">
						<?php echo $model->getAttributeLabel('userDisplay'); ?>
					</td>
					<td>
						<?php echo $model->userDisplay; ?>
					</td>
				</tr>

				<tr>
					<td class="bold">
						<?php echo $model->getAttributeLabel('userAssignedDisplay'); ?>
					</td>
					<td>
						<?php echo $model->userAssignedDisplay; ?>
					</td>
				</tr>
			</tbody>
		</table>
	</div>

	<div class="col-sm-6">
		<table class="table">
			<tbody>
				<tr>
					<td class="bold">
						<?php echo $model->getAttributeLabel('ticket_group_id'); ?>
					</td>
					<td>
						<?php echo isset($model->theGroup) ? EBootstrap::link($model->theGroup->title, array('/ticket/ticketGroup/view', 'id' => $model->theGroup->id)) : ''; ?>
					</td>
				</tr>

				<tr>
					<td class="bold">
						<?php echo $model->getAttributeLabel('created'); ?>
					</td>
					<td>
						<?php echo $model->created; ?>
					</td>
				</tr>

				<tr>
					<td class="bold">
						<?php echo $model->getAttributeLabel('deadline'); ?>
					</td>
					<td>
						<?php echo $model->deadline; ?>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>

<div class="well">
	<?php echo $model->descriptionHtml; ?>
</div>

<?php
$this->endWidget();

if (count($model->childs)) {
	foreach ($model->childs as $child) {
		$this->renderPartial('_view', array('data' => $child));
	}
}

$this->beginWidget(
	'AdminWidget', 
	array(
		'title' => Yii::t('Ticket', 'Antwort schreiben'),
		'icon' => 'plus',
	)
);

$this->renderPartial('_form', array('model' => $ticket));

$this->endWidget();
