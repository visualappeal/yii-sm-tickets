<?php
/* @var $this TicketGroupRuleController */
/* @var $data TicketGroupRule */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ticket_group_id')); ?>:</b>
	<?php echo CHtml::encode($data->ticket_group_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('keyword')); ?>:</b>
	<?php echo CHtml::encode($data->keyword); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('expression')); ?>:</b>
	<?php echo CHtml::encode($data->expression); ?>
	<br />


</div>