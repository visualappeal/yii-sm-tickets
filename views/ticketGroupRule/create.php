<?php
/* @var $this TicketGroupRuleController */
/* @var $model TicketGroupRule */

$this->breadcrumbs=array(
	'Ticket Group Rules'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TicketGroupRule', 'url'=>array('index')),
	array('label'=>'Manage TicketGroupRule', 'url'=>array('admin')),
);
?>

<h1>Create TicketGroupRule</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>