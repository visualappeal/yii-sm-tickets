<?php
/* @var $this TicketGroupRuleController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Ticket Group Rules',
);

$this->menu=array(
	array('label'=>'Create TicketGroupRule', 'url'=>array('create')),
	array('label'=>'Manage TicketGroupRule', 'url'=>array('admin')),
);
?>

<h1>Ticket Group Rules</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
