<?php
/* @var $this TicketGroupRuleController */
/* @var $model TicketGroupRule */

$this->breadcrumbs=array(
	'Ticket Group Rules'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List TicketGroupRule', 'url'=>array('index')),
	array('label'=>'Create TicketGroupRule', 'url'=>array('create')),
	array('label'=>'View TicketGroupRule', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage TicketGroupRule', 'url'=>array('admin')),
);
?>

<h1>Update TicketGroupRule <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>