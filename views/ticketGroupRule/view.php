<?php
/* @var $this TicketGroupRuleController */
/* @var $model TicketGroupRule */

$this->breadcrumbs=array(
	'Ticket Group Rules'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List TicketGroupRule', 'url'=>array('index')),
	array('label'=>'Create TicketGroupRule', 'url'=>array('create')),
	array('label'=>'Update TicketGroupRule', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete TicketGroupRule', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TicketGroupRule', 'url'=>array('admin')),
);
?>

<h1>View TicketGroupRule #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'ticket_group_id',
		'keyword',
		'expression',
	),
)); ?>
