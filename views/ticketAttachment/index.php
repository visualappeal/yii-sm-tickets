<?php
/* @var $this TicketAttachmentController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Ticket Attachments',
);

$this->menu=array(
	array('label'=>'Create TicketAttachment', 'url'=>array('create')),
	array('label'=>'Manage TicketAttachment', 'url'=>array('admin')),
);
?>

<h1>Ticket Attachments</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
