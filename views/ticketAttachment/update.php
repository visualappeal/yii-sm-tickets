<?php
/* @var $this TicketAttachmentController */
/* @var $model TicketAttachment */

$this->breadcrumbs=array(
	'Ticket Attachments'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List TicketAttachment', 'url'=>array('index')),
	array('label'=>'Create TicketAttachment', 'url'=>array('create')),
	array('label'=>'View TicketAttachment', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage TicketAttachment', 'url'=>array('admin')),
);
?>

<h1>Update TicketAttachment <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>