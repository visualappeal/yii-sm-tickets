<?php
/* @var $this TicketAttachmentController */
/* @var $model TicketAttachment */

$this->breadcrumbs=array(
	'Ticket Attachments'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TicketAttachment', 'url'=>array('index')),
	array('label'=>'Manage TicketAttachment', 'url'=>array('admin')),
);
?>

<h1>Create TicketAttachment</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>