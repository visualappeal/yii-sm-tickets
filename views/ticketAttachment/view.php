<?php
/* @var $this TicketAttachmentController */
/* @var $model TicketAttachment */

$this->breadcrumbs=array(
	'Ticket Attachments'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List TicketAttachment', 'url'=>array('index')),
	array('label'=>'Create TicketAttachment', 'url'=>array('create')),
	array('label'=>'Update TicketAttachment', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete TicketAttachment', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TicketAttachment', 'url'=>array('admin')),
);
?>

<h1>View TicketAttachment #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'ticket_id',
		'user_id',
		'file_name',
		'file_extension',
		'file_size',
		'created',
		'updated',
	),
)); ?>
