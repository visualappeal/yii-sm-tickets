<?php

/**
 * This is the model class for the tickets.
 *
 * The followings are the available columns in table '{{ticket}}':
 * @property integer $id
 * @property integer $parent_id
 * @property integer $ticket_group_id
 * @property integer $user_id
 * @property string $user_name
 * @property string $user_email
 * @property integer $user_assigned
 * @property string $title
 * @property string $description
 * @property string $created
 * @property string $updated
 * @property string $deadline
 * @property integer $status
 * @property intger $ticket_api_id
 *
 * The followings are the available model relations:
 * @property TicketGroup $ticketGroup
 * @property Ticket $parent
 * @property Ticket[] $tickets
 * @property TicketAttachment[] $ticketAttachments
 * @property TicketApi $ticketApi
 *
 * @package Ticket
 */
class Ticket extends CActiveRecord
{
	/**
	 * Ticket status constants.
	 */
	const STATUS_NEW = 0;

	const STATUS_OPEN = 100;

	const STATUS_ASSIGNED = 200;

	const STATUS_CLOSED = 300;

	/**
	 * Returns the static model of the specified AR class.
	 *
	 * @param string $className active record class name.
	 *
	 * @access public
	 * @return Ticket the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Get the table name for the model
	 *
	 * @access public
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{ticket}}';
	}

	/**
	 * Get the attribute validation rules.
	 *
	 * @access public
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('title', 'required'),
			array('ticket_group_id, status, parent_id, user_id, user_assigned, ticket_api_id', 'numerical', 'integerOnly' => true),
			array('user_name', 'length', 'max' => 100),
			array('user_email', 'length', 'max' => 150),
			array('title', 'length', 'max' => 255),
			array('created, updated, deadline', 'date', 'format' => 'yyyy-M-d H:m:s'),
			array('ticket_api_id', 'default', 'value' => 1),
			array('description', 'safe'),

			array('id, parent_id, ticket_group_id, user_id, user_name, user_email, user_assigned, title, description, created, updated, deadline, status', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * Get the ticket relations.
	 *
	 * @access public
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'theGroup' => array(self::BELONGS_TO, 'TicketGroup', 'ticket_group_id'),
			'theParent' => array(self::BELONGS_TO, 'Ticket', 'parent_id'),
			'theUser' => array(self::BELONGS_TO, 'User', 'user_id'),
			'theUserAssigned' => array(self::BELONGS_TO, 'User', 'user_assigned'),
			'theApi' => array(self::BELONGS_TO, 'TicketApi', 'ticket_api_id'),
			'childs' => array(self::HAS_MANY, 'Ticket', 'parent_id'),
			'attachments' => array(self::HAS_MANY, 'TicketAttachment', 'ticket_id'),
		);
	}

	/**
	 * Get the default model scope.
	 *
	 * @access public
	 * @return array
	 */
	public function defaultScope()
	{
		$t = $this->getTableAlias(false, false);

		return array(
			'order' => "$t.status ASC, $t.deadline ASC",
		);
	}

	/**
	 * Get the model scopes.
	 *
	 * @access public
	 * @return array
	 */
	public function scopes()
	{
		$t = $this->getTableAlias(false, false);

		return array(
			'root' => array(
				'condition' => "$t.parent_id IS NULL",
			),
		);
	}

	/**
	 * After find method. Convert the date format.
	 *
	 * @access public
	 * @return boolean
	 */
	public function afterFind()
	{
		$this->created = date('d.m.Y H:i:s', strtotime($this->created));

		if (!empty($this->updated) and $this->updated != '0000-00-00 00:00:00')
			$this->updated = date('d.m.Y H:i:s', strtotime($this->updated));
		else
			$this->updated = null;

		if (!empty($this->deadline) and $this->deadline != '0000-00-00 00:00:00')
			$this->deadline = date('d.m.Y H:i:s', strtotime($this->deadline));
		else
			$this->deadline = null;
	}

	/**
	 * Before validate method. Convert the date format.
	 *
	 * @access public
	 * @return boolean
	 */
	public function beforeValidate()
	{
		if (parent::beforeValidate()) {
			if (!empty($this->created) and strtotime($this->created) !== false) {
				$this->created = date('Y-m-d H:i:s', strtotime($this->created));
			}

			if (!empty($this->updated) and strtotime($this->updated) !== false) {
				$this->updated = date('Y-m-d H:i:s', strtotime($this->updated));
			}

			if (!empty($this->deadline) and strtotime($this->deadline) !== false) {
				$this->deadline = date('Y-m-d H:i:s', strtotime($this->deadline));
			}
		}

		return parent::beforeValidate();
	}

	/**
	 * After save method. Send email to the subscribers.
	 *
	 * @access public
	 * @return void
	 */
	public function afterSave()
	{
		if ($this->isNewRecord) {
			if (empty($this->parent_id)) {
				//New ticket

				//Send confirmation
				$mail = new Email;
				if (isset($this->theUser)) {
					$to = array(
						$this->theUser->email => $this->theUser->name
					);
				} else {
					$to = array(
						$this->user_email => $this->user_name
					);
				}

				$html = '<h2>' . Yii::t('Ticket.EmailConfirmation', 'Das Ticket wurde erstellt') . '</h2>';
				$html .= '<p>' . Yii::t('Ticket.EmailConfirmation', 'Du hast das Ticket "{title}" erstellt.', array('{title}' => $this->title)) . '</p>';
				$html .= '<p>' . nl2br($this->description) . '</p>';

				$plain = Yii::t('Ticket.EmailConfirmation', 'Das Ticket wurde erstellt');
				$plain .= "\n\n" . Yii::t('Ticket.EmailConfirmation', 'Du hast das Ticket "{title}" erstellt.', array('{title}' => $this->title));
				$plain .= "\n\n" . $this->description;

				if (Yii::app()->user->checkAccess(User::LEVEL_BOARD)) {
					$html .= '<p>' . EBootstrap::link(Yii::t('Ticket.EmailConfirmation', 'Ticket ansehen'), Yii::app()->createAbsoluteUrl('/ticket/ticket/view', array('id' => $this->id))) . '</p>';
					$plain .= "\n\n" . Yii::t('Ticket.EmailConfirmation', 'Ticket ansehen') . ': ' . Yii::app()->createAbsoluteUrl('/ticket/ticket/view', array('id' => $this->id));
				}

				$mail->send($to, Yii::t('Ticket.EmailConfirmation', 'Ticket erstellt'), $html, $plain);

				//Send mail to admin
				$mail = new Email;
				$to = array(
					Yii::app()->params['adminEmail'] => Yii::app()->name
				);

				$html = '<h2>' . Yii::t('Ticket.EmailConfirmation', 'Ein neues Ticket wurde erstellt') . '</h2>';
				$html .= '<p>' . Yii::t('Ticket.EmailConfirmation', 'Es wurde das Ticket "{title}" erstellt.', array('{title}' => $this->title)) . '</p>';
				$html .= '<p>' . nl2br($this->description) . '</p>';

				$plain = Yii::t('Ticket.EmailConfirmation', 'Ein neues Ticket wurde erstellt');
				$plain .= "\n\n" . Yii::t('Ticket.EmailConfirmation', 'Es wurde das Ticket "{title}" erstellt.', array('{title}' => $this->title));
				$plain .= "\n\n" . $this->description;

				$html .= '<p>' . EBootstrap::link(Yii::t('Ticket.EmailConfirmation', 'Ticket ansehen'), Yii::app()->createAbsoluteUrl('/ticket/ticket/view', array('id' => $this->id))) . '</p>';
				$plain .= "\n\n" . Yii::t('Ticket.EmailConfirmation', 'Ticket ansehen') . ': ' . Yii::app()->createAbsoluteUrl('/ticket/ticket/view', array('id' => $this->id));

				$mail->send($to, Yii::t('Ticket.EmailConfirmation', 'Ticket erstellt'), $html, $plain);
			} else {
				//Response
				if (isset($this->theParent)) {
					$mail = new Email;
					if (isset($this->theParent->theUser)) {
						$to = array(
							$this->theParent->theUser->email => $this->theParent->theUser->name
						);
					} else {
						$to = array(
							$this->theParent->user_email => $this->theParent->user_name
						);
					}

					$html = '<h2>' . Yii::t('Ticket.EmailResponse', 'Antwort auf Ticket "{title}"', array('{title}' => $this->theParent->title)) . '</h2>';
					$html .= '<p>' . Yii::t('Ticket.EmailResponse', '{user} hat auf dein Ticket "{title}" geantwortet:', array('{user}' => $this->userDisplay, '{title}' => $this->theParent->title)) . '</p>';
					$html .= '<p>' . $this->description . '</p>';

					$plain = Yii::t('Ticket.EmailResponse', 'Antwort auf Ticket "{title}"', array('{title}' => $this->theParent->title)) . "\n\n";
					$plain .= Yii::t('Ticket.EmailResponse', '{user} hat auf dein Ticket "{title}" geantwortet:', array('{user}' => $this->userDisplay, '{title}' => $this->theParent->title)) . "\n\n";
					$plain .= $this->description;

					if (isset($this->theParent->theUser) and ($this->theParent->theUser->level >= User::LEVEL_BOARD)) {
						$html .= '<p>' . EBootstrap::link(Yii::t('Ticket.EmailConfirmation', 'Ticket ansehen'), Yii::app()->createAbsoluteUrl('/ticket/ticket/view', array('id' => $this->theParent->id, '#' => $this->id))) . '</p>';
						$plain .= "\n\n" . Yii::t('Ticket.EmailConfirmation', 'Ticket ansehen') . ': ' . Yii::app()->createAbsoluteUrl('/ticket/ticket/view', array('id' => $this->theParent->id, '#' => $this->id));
					}

					$mail->send($to, Yii::t('Ticket.EmailResponse', 'Antwort auf Ticket #{id}', array('{id}' => $this->theParent->id)), $html, $plain);
				}
			}
		}

		return parent::afterSave();
	}

	/**
	 * Get the user string for the ticket.
	 *
	 * @access public
	 * @return string
	 */
	public function getUserDisplay()
	{
		if (isset($this->theUser)) {
			return $this->theUser->name . ' (' . $this->theUser->email . ')';
		} elseif (!empty($this->user_email)) {
			return $this->user_name . ' (' . $this->user_email . ')';
		} else {
			return '';
		}
	}

	public function getUserAssignedDisplay()
	{
		if (isset($this->theUserAssigned)) {
			return $this->theUserAssigned->name . ' (' . $this->theUserAssigned->email . ')';
		} else {
			return Yii::t('Ticket', 'Niemand');
		}
	}

	/**
	 * Get the string for the status message.
	 *
	 * @access public
	 * @return string
	 */
	public function getStatusMessage()
	{
		switch ($this->status) {
			case self::STATUS_NEW:
				return Yii::t('Ticket', 'Neu');
			case self::STATUS_OPEN:
				return Yii::t('Ticket', 'Offen');
			case self::STATUS_ASSIGNED:
				return Yii::t('Ticket', 'Zugewiesen');
			case self::STATUS_CLOSED:
				return Yii::t('Ticket', 'Geschlossen');
			default:
				return Yii::t('Ticket', 'Unbekannt');
		}
	}

	/**
	 * Get dropdown values for the status dropdown.
	 *
	 * @access public
	 * @return array
	 */
	public function getStatusDropdown()
	{
		return array(
			self::STATUS_NEW => Yii::t('Ticket', 'Neu'),
			self::STATUS_OPEN => Yii::t('Ticket', 'Offen'),
			self::STATUS_ASSIGNED => Yii::t('Ticket', 'Zugewiesen'),
			self::STATUS_CLOSED => Yii::t('Ticket', 'Geschlossen'),
		);
	}

	/**
	 * Get all groups for a HTML dropdown.
	 *
	 * @access public
	 * @return array
	 */
	public function getGroups()
	{
		return CHtml::listData(TicketGroup::model()->findAll(), 'id', 'title');
	}

	/**
	 * Set user.
	 *
	 * @param integer $id User ID
	 *
	 * @access public
	 * @return void
	 */
	public function setUser($id)
	{
		$this->user_id = $id;
	}

	/**
	 * Set ticket as response.
	 *
	 * @param $model Ticket Parent model
	 *
	 * @access public
	 * @return void
	 */
	public function setParent(Ticket $model) {
		$this->parent_id = $model->id;
		$this->ticket_group_id = $model->ticket_group_id;
		$this->title = Yii::t('Ticket', 'Bezug: #{id}', array('{id}' => $model->id));
	}

	/**
	 * Get the attribute labels.
	 *
	 * @access public
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('Ticket', 'ID'),
			'parent_id' => Yii::t('Ticket', 'Zugehörig'),
			'ticket_group_id' => Yii::t('Ticket', 'Kategorie'),
			'user_id' => Yii::t('Ticket', 'Benutzer'),
			'userDisplay' => Yii::t('Ticket', 'Benutzer'),
			'user_name' => Yii::t('Ticket', 'Name (Gast)'),
			'user_email' => Yii::t('Ticket', 'E-Mail (Gast)'),
			'user_assigned' => Yii::t('Ticket', 'Verantwortlich'),
			'userAssignedDisplay' => Yii::t('Ticket', 'Verantwortlich'),
			'title' => Yii::t('Ticket', 'Titel'),
			'description' => Yii::t('Ticket', 'Beschreibung'),
			'created' => Yii::t('Ticket', 'Erstellt'),
			'updated' => Yii::t('Ticket', 'Bearbeitet'),
			'deadline' => Yii::t('Ticket', 'Deadline'),
			'status' => Yii::t('Ticket', 'Status'),
			'statusMessage' => Yii::t('Ticket', 'Status'),
			'ticket_api_id' => Yii::t('Ticket', 'API'),
		);
	}

	/**
	 * Strip tags and add breaks into the description.
	 *
	 * @access public
	 * @return string
	 */
	public function getDescriptionHtml()
	{
		return nl2br(strip_tags($this->description));
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * @access public
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		$t = $this->getTableAlias();

		$criteria = new CDbCriteria;

		$criteria->compare("$t.id", $this->id);
		$criteria->addCondition("$t.parent_id IS NULL");
		$criteria->compare("$t.ticket_group_id", $this->ticket_group_id);
		$criteria->compare("$t.user_id", $this->user_id);
		$criteria->compare("$t.user_name", $this->user_name, true);
		$criteria->compare("$t.user_email", $this->user_email, true);
		$criteria->compare("$t.user_assigned", $this->user_assigned);
		$criteria->compare("$t.title", $this->title, true);
		$criteria->compare("$t.created", $this->created, true);
		$criteria->compare("$t.status", $this->status);

		return new CActiveDataProvider(
			$this, 
			array(
				'criteria' => $criteria,
			)
		);
	}
}