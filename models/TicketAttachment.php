<?php

/**
 * This is the model class for table "{{ticket_attachment}}".
 *
 * The followings are the available columns in table '{{ticket_attachment}}':
 * @property string $id
 * @property string $ticket_id
 * @property string $user_id
 * @property string $file_name
 * @property string $file_extension
 * @property string $file_size
 * @property string $created
 * @property string $updated
 *
 * The followings are the available model relations:
 * @property Ticket $ticket
 */
class TicketAttachment extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return TicketAttachment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{ticket_attachment}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ticket_id, user_id, file_name, file_extension, file_size, created', 'required'),
			array('ticket_id, user_id, file_size', 'length', 'max'=>10),
			array('file_name', 'length', 'max'=>100),
			array('file_extension', 'length', 'max'=>50),
			array('updated', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, ticket_id, user_id, file_name, file_extension, file_size, created, updated', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'ticket' => array(self::BELONGS_TO, 'Ticket', 'ticket_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'ticket_id' => 'Ticket',
			'user_id' => 'User',
			'file_name' => 'File Name',
			'file_extension' => 'File Extension',
			'file_size' => 'File Size',
			'created' => 'Created',
			'updated' => 'Updated',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('ticket_id',$this->ticket_id,true);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('file_name',$this->file_name,true);
		$criteria->compare('file_extension',$this->file_extension,true);
		$criteria->compare('file_size',$this->file_size,true);
		$criteria->compare('created',$this->created,true);
		$criteria->compare('updated',$this->updated,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}