<?php

/**
 * This is the model class for table "{{ticket_group_rule}}".
 *
 * The followings are the available columns in table '{{ticket_group_rule}}':
 * @property string $id
 * @property integer $ticket_group_id
 * @property string $keyword
 * @property string $expression
 *
 * The followings are the available model relations:
 * @property TicketGroup $ticketGroup
 */
class TicketGroupRule extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return TicketGroupRule the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{ticket_group_rule}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ticket_group_id', 'required'),
			array('ticket_group_id', 'numerical', 'integerOnly'=>true),
			array('keyword, expression', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, ticket_group_id, keyword, expression', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'ticketGroup' => array(self::BELONGS_TO, 'TicketGroup', 'ticket_group_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'ticket_group_id' => 'Ticket Group',
			'keyword' => 'Keyword',
			'expression' => 'Expression',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('ticket_group_id',$this->ticket_group_id);
		$criteria->compare('keyword',$this->keyword,true);
		$criteria->compare('expression',$this->expression,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}