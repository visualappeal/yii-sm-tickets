<?php

/**
 * This is the model class for table "{{ticket_group}}".
 *
 * The followings are the available columns in table '{{ticket_group}}':
 * @property integer $id
 * @property string $title
 * @property string $description
 *
 * The followings are the available model relations:
 * @property Ticket[] $tickets
 * @property TicketGroupRule[] $ticketGroupRules
 *
 * @package Ticket
 * @subpackage Group
 */
class TicketGroup extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 *
	 * @param string $className active record class name.
	 *
	 * @access public
	 * @return TicketGroup the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Get the table name for the model.
	 *
	 * @access public
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{ticket_group}}';
	}

	/**
	 * Get the attribute validation rules.
	 *
	 * @access public
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('title', 'required'),
			array('title', 'length', 'max' => 50),
			array('description', 'safe'),
			array('id, title', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * Get the model relations.
	 *
	 * @access public
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			'tickets' => array(self::HAS_MANY, 'Ticket', 'ticket_group_id'),
			'rules' => array(self::HAS_MANY, 'TicketGroupRule', 'ticket_group_id'),
		);
	}

	/**
	 * Get the attribute labels.
	 *
	 * @access public
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('Ticket.Group', 'ID'),
			'title' => Yii::t('Ticket.Group', 'Title'),
			'description' => Yii::t('Ticket.Group', 'Beschreibung'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * @access public
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);

		return new CActiveDataProvider(
			$this, 
			array(
				'criteria' => $criteria,
			)
		);
	}
}