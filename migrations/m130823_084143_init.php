<?php

/**
 * Erste Migration um notwendige Tabellen zu erstellen.
 */
class m130823_084143_init extends CDbMigration
{
	public function safeUp()
	{
		$this->createTable(
			'{{ticket_api}}',
			array(
				'id' => 'int(11) NOT NULL AUTO_INCREMENT',
				'title' => 'varchar(50) DEFAULT NULL',
				'PRIMARY KEY (`id`)',
			)
		);

		$this->createTable(
			'{{ticket_group}}',
			array(
				'id' => 'smallint(5) unsigned NOT NULL AUTO_INCREMENT',
				'title' => 'varchar(50) NOT NULL',
				'description' => 'text',
				'PRIMARY KEY (`id`)',
			)
		);

		$this->createTable(
			'{{ticket_group_rule}}',
			array(
				'id' => 'int(10) unsigned NOT NULL AUTO_INCREMENT',
				'ticket_group_id' => 'smallint(5) unsigned NOT NULL',
				'keyword' => 'varchar(100) DEFAULT NULL',
				'expression' => 'varchar(100) DEFAULT NULL',
				'PRIMARY KEY (`id`)',
				'KEY `ticket_group_id` (`ticket_group_id`)',
			)
		);

		$this->createTable(
			'{{ticket}}',
			array(
				'id' => 'int(10) unsigned NOT NULL AUTO_INCREMENT',
				'parent_id' => 'int(10) unsigned DEFAULT NULL',
				'ticket_group_id' => 'smallint(5) unsigned DEFAULT NULL',
				'user_id' => 'int(10) unsigned DEFAULT NULL',
				'user_name' => 'varchar(100) DEFAULT NULL COMMENT "Falls Benutzer anonym"',
				'user_email' => 'varchar(150) DEFAULT NULL COMMENT "Falls Benutzer anonym"',
				'user_assigned' => 'int(10) unsigned DEFAULT NULL',
				'title' => 'varchar(255) NOT NULL',
				'description' => 'text',
				'created' => 'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP',
				'updated' => 'timestamp NULL DEFAULT NULL',
				'deadline' => 'timestamp NULL DEFAULT NULL',
				'status' => 'smallint(5) unsigned DEFAULT "0"',
				'ticket_api_id' => 'int(11) NOT NULL',
				'PRIMARY KEY (`id`)',
				'KEY `parent_id` (`parent_id`)',
				'KEY `ticket_group_id` (`ticket_group_id`)',
				'KEY `user_id` (`user_id`)',
				'KEY `ticket_api_id` (`ticket_api_id`)',
			)
		);

		$this->createTable(
			'{{ticket_attachment}}',
			array(
				'id' => 'int(10) unsigned NOT NULL AUTO_INCREMENT',
				'ticket_id' => 'int(10) unsigned NOT NULL',
				'user_id' => 'int(10) unsigned NOT NULL',
				'file_name' => 'varchar(100) NOT NULL',
				'file_extension' => 'varchar(50) NOT NULL',
				'file_size' => 'int(10) unsigned NOT NULL',
				'created' => 'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP',
				'updated' => 'timestamp NULL DEFAULT NULL',
				'PRIMARY KEY (`id`)',
				'KEY `ticket_id` (`ticket_id`)',
				'KEY `user_id` (`user_id`)',
			)
		);
	}

	public function safeDown()
	{
		$this->dropTable('{{ticket_attachment}}');
		$this->dropTable('{{ticket}}');
		$this->dropTable('{{ticket_group_rule}}');
		$this->dropTable('{{ticket_group}}');
		$this->dropTable('{{ticket_api}}');
	}
}