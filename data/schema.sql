SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `cvjm` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `cvjm` ;

-- -----------------------------------------------------
-- Table `cvjm`.`ticket_group`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `cvjm`.`ticket_group` (
  `id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `title` VARCHAR(50) NOT NULL ,
  `description` TEXT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cvjm`.`ticket_api`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `cvjm`.`ticket_api` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `title` VARCHAR(50) NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cvjm`.`ticket`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `cvjm`.`ticket` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `parent_id` INT UNSIGNED NULL ,
  `ticket_group_id` SMALLINT UNSIGNED NULL ,
  `user_id` INT UNSIGNED NULL ,
  `user_name` VARCHAR(100) NULL COMMENT 'Falls Benutzer anonym' ,
  `user_email` VARCHAR(150) NULL COMMENT 'Falls Benutzer anonym' ,
  `user_assigned` INT UNSIGNED NULL ,
  `title` VARCHAR(255) NOT NULL ,
  `description` TEXT NULL ,
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `updated` TIMESTAMP NULL ,
  `deadline` TIMESTAMP NULL ,
  `status` SMALLINT UNSIGNED NULL DEFAULT 0 ,
  `ticket_api_id` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_ticket_ticket_idx` (`parent_id` ASC) ,
  INDEX `fk_ticket_ticket_group_idx` (`ticket_group_id` ASC) ,
  INDEX `fk_ticket_ticket_api1_idx` (`ticket_api_id` ASC) ,
  CONSTRAINT `fk_ticket_ticket`
    FOREIGN KEY (`parent_id` )
    REFERENCES `cvjm`.`ticket` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ticket_ticket_group`
    FOREIGN KEY (`ticket_group_id` )
    REFERENCES `cvjm`.`ticket_group` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ticket_ticket_api1`
    FOREIGN KEY (`ticket_api_id` )
    REFERENCES `cvjm`.`ticket_api` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cvjm`.`ticket_attachment`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `cvjm`.`ticket_attachment` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `ticket_id` INT UNSIGNED NOT NULL ,
  `user_id` INT UNSIGNED NOT NULL ,
  `file_name` VARCHAR(100) NOT NULL ,
  `file_extension` VARCHAR(50) NOT NULL ,
  `file_size` INT UNSIGNED NOT NULL ,
  `created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `updated` TIMESTAMP NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_ticket_attachment_ticket1_idx` (`ticket_id` ASC) ,
  CONSTRAINT `fk_ticket_attachment_ticket`
    FOREIGN KEY (`ticket_id` )
    REFERENCES `cvjm`.`ticket` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `cvjm`.`ticket_group_rule`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `cvjm`.`ticket_group_rule` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `ticket_group_id` SMALLINT UNSIGNED NOT NULL ,
  `keyword` VARCHAR(100) NULL ,
  `expression` VARCHAR(100) NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_ticket_group_rule_ticket_group1_idx` (`ticket_group_id` ASC) ,
  CONSTRAINT `fk_ticket_group_rule_ticket_group`
    FOREIGN KEY (`ticket_group_id` )
    REFERENCES `cvjm`.`ticket_group` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

USE `cvjm` ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `cvjm`.`ticket_api`
-- -----------------------------------------------------
START TRANSACTION;
USE `cvjm`;
INSERT INTO `cvjm`.`ticket_api` (`id`, `title`) VALUES (1, 'Default');
INSERT INTO `cvjm`.`ticket_api` (`id`, `title`) VALUES (2, 'Contact Form');
INSERT INTO `cvjm`.`ticket_api` (`id`, `title`) VALUES (3, 'Website Widget');

COMMIT;